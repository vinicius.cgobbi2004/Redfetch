#!/usr/bin/env bash
clear
Menu(){
	echo
	echo "Não se preocupe, em dar acesso ao sudo quando pedido, não tem nada de prejudicial aqui. Sinta-se avontade para ler o código fonte, e contribuir: https://github.com/RedsonBr140/Redfetch/edit/main/install.sh"
	echo
	echo "-----------------------"
	echo "     Redfetch install  "
	echo "-----------------------"
	echo
	echo "[ 1 ] Arch & based"
	echo "[ 2 ] Ubuntu & based"
	echo "[ 3 ] Fedora"
	echo "[ 4 ] Sair do instalador"
	echo
	echo -n "Qual sua distro Linux? "
	read opcao
	case $opcao in
	1) Arch_Menu;;
	2) Ubuntu_Menu ;;
	3) Fedora;;
	4) exit ;;
	*) Menu ;;
esac
}

Arch_Menu() {
	clear
	echo "---------------"
	echo "  Arch Based   "
	echo "---------------"
	echo
	echo "[ 1 ] Arch Linux"
	echo "[ 2 ] Manjaro Linux"
	echo "[ 3 ] Endeavour OS"
	echo "[ 4 ] Voltar ao inicio"
	echo
	echo -n "Qual sua distro? "
	read arch_opcao
	case $arch_opcao in
	1) Arch ;;
	2) Manjaro ;;
	3) Endeavour ;;
	4) Retornar ;;
	*) Opção_desconhecida
esac
}

Retornar() {
	clear
	Menu
}

Opção_desconhecida() {
	echo 
	echo "Erro!! opção não encontrada, voltando a tela inicial"
	Menu
}

Arch() {
sudo pacman --noconfirm -Sy curl
curl -o redfetch.sh https://raw.githubusercontent.com/RedsonBr140/Redfetch/master/redfetch-arch?token=ARBPXUVZ2Y4IKNQVK2XVLLTAJZB7I
mv redfetch.sh redfetch
sudo mv redfetch /usr/bin/redfetch
sudo chmod +x /usr/bin/redfetch
sudo pacman --noconfirm -Sy lolcat
}

Manjaro() {
	sudo pacman --noconfirm -Sy curl
	curl -o redfetch.sh https://raw.githubusercontent.com/RedsonBr140/Redfetch/master/redfetch-manjaro?token=ARBPXUTPBYRSYUHRDOJIL5DAJZCHY
	mv redfetch.sh redfetch
	sudo mv redfetch /usr/bin/redfetch
	sudo chmod +x /usr/bin/redfetch
	sudo pacman --noconfirm -Sy lolcat
}
Endeavour() {
sudo pacman --noconfirm -Sy curl
curl -o redfetch.sh https://raw.githubusercontent.com/RedsonBr140/Redfetch/master/redfetch-endeavour?token=ARBPXURXUQYYHZLRLS7YAGDAJXZP4
mv redfetch.sh redfetch
sudo mv redfetch /usr/bin/redfetch
sudo chmod +x /usr/bin/redfetch
sudo pacman --noconfirm -Sy lolcat
}

Ubuntu_Menu() {
	clear
	echo "-------------"
	echo "Ubuntu Based "
	echo "-------------"
	echo
	echo "[ 1 ] Ubuntu"
	echo "[ 2 ] Mint"
	echo "[ 3 ] Kubuntu"
	echo "[ 4 ] Voltar ao inicio"
	echo
	echo -n "Qual sua distro? "
	read ubuntu_opcao
	case $ubuntu_opcao in
	1) Ubuntu ;;
	2) Mint ;;
	3) Kubuntu ;;
	4) Retornar ;;
	*) Opção_desconhecida ;;
	esac
}

Ubuntu() {
    sudo apt install lolcat curl -y
    curl -o redfetch.sh https://raw.githubusercontent.com/RedsonBr140/Redfetch/master/redfetch-ubuntu?token=ARBPXUQEVUBZIBA4ADE7M43AJZCKM
    mv redfetch.sh redfetch
    sudo mv redfetch /usr/bin/redfetch
    sudo chmod +x /usr/bin/redfetch
}

Mint() {
	sudo apt install lolcat curl -y
    curl -o redfetch.sh https://raw.githubusercontent.com/RedsonBr140/Redfetch/master/redfetch-mint?token=ARBPXUQ34J6QXYUBCHIRWNTAJZCMG
    mv redfetch.sh redfetch
    sudo mv redfetch /usr/bin/redfetch
    sudo chmod +x /usr/bin/redfetch
}
Kubuntu() {
	sudo apt install lolcat curl -y
    curl -o redfetch.sh https://raw.githubusercontent.com/RedsonBr140/Redfetch/master/redfetch-kubuntu?token=ARBPXUS6NJHURPFEBJROEATAJZCFC
    mv redfetch.sh redfetch
    sudo mv redfetch /usr/bin/redfetch
    sudo chmod +x /usr/bin/redfetch
}

Fedora() {
	sudo dnf install lolcat curl -y
	curl -o redfetch.sh https://raw.githubusercontent.com/Androwinbr/Redfetch/main/redfetch-fedora
	mv redfetch.sh redfetch
	sudo mv redfetch /usr/bin/redfetch
	sudo chmod +x /usr/bin/redfetch
}
Menu
