<p align="center">
<img src="https://github.com/RedsonBr140/Redfetch/raw/main/redfetch_logo-removebg-preview.png" width="320" height="320"/>
<br>
<h1 align="center">Redfetch - Apenas mais um fetch.</h1>
<p align="center"><strong>Atenção!</strong> Redfetch não receberá atualizações, e foi arquivado a favor do <a href="https://gitlab.com/pedro.portales/ppfetch">ppfetch</a>.</p>
</p>

<p align="center"><img src="https://github.com/RedsonBr140/Redfetch/raw/main/redfetch-arch.png"/></p>

Este é o Redfetch no Arch, a ideia foi criar um fetch bem minimalista, mas que mostrasse o máximo de informações necessárias, atualmente o Redfetch está disponível para:
- Arch Linux
- EndeavourOS
- Manjaro
- Ubuntu
- Kubuntu
- Linux Mint

ele depende do `lolcat` para ser usado, e do `Curl` para ser instalado, e essas duas dependências são instaladas automaticamente pelo script. Estando de acordo, vamos continuar:

---

Primeiramente, você precisa baixar o Script, com o seguinte comando:
```sh
curl -o install.sh https://raw.githubusercontent.com/RedsonBr140/Redfetch/master/install.sh
```
depois disso, é só dar permissão e executar:
```bash
chmod +x install.sh && ./install.sh
```
---
![Tela de instalação do Redfetch](https://media.discordapp.net/attachments/776201664902201346/803256838064963624/unknown.png)

O Script apresenta uma tela bem simples, onde você pode escolher a partir de um número o seu sistema, 1 para Arch, 2 para Ubuntu, e conforme o projeto for expandindo, terão bem mais distros, ao fim do processo, ele não exibirá nenhuma mensagem de concluido, se não tiver erros, então considere que deu certo.

![Print mostrando que deu certo!](https://media.discordapp.net/attachments/776201664902201346/803258122700980245/unknown.png?width=642&height=460)

Bem, o Redfetch foi instalado com sucesso nessa imagem, para atualiza-lo, é só rodar o script novamente, como se estivesse instalando pela primeira vez.

# Agradecimentos
 - [@Androwinbr](https://github.com/Androwinbr)
